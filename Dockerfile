FROM gradle:6.9.0-jdk8 as builder
COPY build.gradle.kts .
COPY src ./src
RUN gradle clean build
#CMD gradle clean build

FROM openjdk:8-jre-alpine
EXPOSE 8080
COPY --from=builder /home/gradle/build/libs/gradle-0.0.1-SNAPSHOT.jar /jarjar.jar
#RUN java -jar /jarjar.jar

#ENTRYPOINT [ "tail", "-f", "/dev/null" ]
ENTRYPOINT [ "java", "-jar", "/jarjar.jar" ]